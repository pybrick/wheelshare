package com.cccwheelshare;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Encrypt
{
    final private static String DEFAULT  = "sha-512";
          private static String SELECTED = DEFAULT;

    final public static byte[] hash( final String text, final String alg ) throws NoSuchAlgorithmException
    {
        SELECTED = alg;
        return hash( text );
    }
    
    final public static String hashString( final String text ) throws NoSuchAlgorithmException 
    {
        return toString( hash( text ) );
    }
    
    final public static byte[] hash( final String text ) throws NoSuchAlgorithmException
    {
        return MessageDigest.getInstance( SELECTED ).digest( text.getBytes() );
    }
    
    final public static String toString( final byte[] byteData ) 
    {
        return ( new BigInteger( 1, byteData ) ).toString( 16 );
    }
    
    final public static Boolean verify( final String lhs, final String rhs ) throws NoSuchAlgorithmException
    {
        Data.verbose = lhs+Data.nl+hashString( lhs )+Data.nl+rhs;
        return hashString( lhs ).equals( rhs );
    }
}
