package com.cccwheelshare;

import com.google.android.gcm.GCMRegistrar;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.widget.EditText;

public class Helper extends Application
{
    public static AlertDialog createAlertDialog( AlertDialog.Builder listBuilder,
                                                 final CharSequence[] items,
                                                 final String title,
                                                 final EditText editText )
    {
        listBuilder.setTitle( title );
        listBuilder.setItems(
            items,
            new DialogInterface.OnClickListener()
            {
                public void onClick( DialogInterface dialog, int item )
                {
                    editText.setText( items[item] );
                }
            }
        );
        
        return listBuilder.create();
    }
    
    /*** Device registration ***/
    public static String registerDevice( Context context )
    {
        Resources r = context.getResources();
        String status = Data.Empty;
        status += "\n"+r.getString( R.string.Device_ID )+Data.SENDER_ID;
    
        GCMRegistrar.checkDevice( context );
        GCMRegistrar.checkManifest( context );
    
        final String regId = GCMRegistrar.getRegistrationId( context );
        status += "\n"+r.getString( R.string.Device_ID_GCM )+regId;
          
        if( regId.equals( Data.Empty ) )
        {
            GCMRegistrar.register( context, Data.SENDER_ID );
            status += "\n"+r.getString( R.string.Device_Registered );
        }
        else
            status += "\n"+r.getString( R.string.Device_Already_Registered );
          
        return status;
    }
}