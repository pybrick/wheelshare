package com.cccwheelshare;

import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Font
{
          public  static Boolean status = false;
    final public  static String  Mangal = "fonts/mangal.ttf";
    
    final public static void setAppFont( ViewGroup parent )
    {
        if( status )
        {
            setAppFont( parent, Mangal );
        }
        else
        {
            setAppFont( parent, Typeface.SERIF );
        }
    }
    
    final private static void setAppFont( ViewGroup view, final String font )
    {
        setAppFont( view, Typeface.createFromAsset( view.getContext().getAssets(), font ) );  
    }
    
    final private static void setAppFont( ViewGroup parent, final Typeface font )
    {
        if( parent == null || font == null )
        {
            return;
        }
        
        for( int i = 0; i < parent.getChildCount(); ++i )
        {
            final View child = parent.getChildAt( i );
            if( child instanceof TextView )
            {
                ( (TextView) child ).setTypeface( font );
            }
            else if( child instanceof ViewGroup )
            {
                setAppFont( (ViewGroup) child, font );
            }
        }
    }
    
    public static String Text( final View v )
    {
        if( v instanceof TextView )
        {
            return ( (TextView) v ).getText().toString().trim();
        }
        else
        {
            return Data.Empty;
        }
    }
    
    public static void Text( final View v, final String text )
    {
        if( v instanceof TextView )
        {
            ( (TextView) v ).setText( text );
        }
    }
}
