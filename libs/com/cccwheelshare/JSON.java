package com.cccwheelshare;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;

public class JSON
{
    private static JSONObject data     = new JSONObject();
    private        JSONObject itemized = new JSONObject();
    private static JSONObject json     = new JSONObject();
    private static JSONObject result   = new JSONObject();
    private static Boolean    success  = null;              // Remember to check for null!
    private static String     url      = Data.Empty;
    private static String     gcmType  = Data.Empty;
    private static Integer    userId   = -1;
    private static ProgressDialog progress;

    public static ProgressDialog PROGRESS()
    {
        return progress;
    }
    public static void PROGRESS( final Context context, final String message )
    {
        progress = new ProgressDialog( context );
        progress.setMessage( message );
        progress.setIndeterminate( true );
        progress.setCancelable( true );
        progress.setCanceledOnTouchOutside( true );
//      progress.setProgressStyle( ProgressDialog.STYLE_HORIZONTAL );
    }
    
    public JSONObject execute( final String url, final String gcmType ) throws ClientProtocolException, JSONException, IOException
    {
        JSON.gcmType = gcmType;
        return execute( url );
    }

    public JSONObject execute( final String url, final JSONObject data ) throws ClientProtocolException, JSONException, IOException
    {
        JSON.data = data;
        return execute( url );
    }

    public JSONObject execute( final String url, final Integer userId ) throws ClientProtocolException, JSONException, IOException
    {
        JSON.userId = userId;
        return execute( url );
    }
    
    public JSONObject execute( final String url ) throws JSONException, ClientProtocolException, IOException
    {
        JSON.url = url;
        
        // User Login
        if( url.equals( Data.URL.Verified_User_FindByEmail ) ||
            url.equals( Data.URL.Unverified_User_FindByEmail ) ||
            url.equals( Data.URL.User_SendVerificationEmail ) )
        {
            json = Data.currentUser.tojson();
//            Data.currentUser = new User();
            getResult();
        }
        
        // User Info
        else if( url.equals( Data.URL.getUser ) )
        {
            json.put( Data.FIELD.userId, Data.selectedUser.ID() );
            if( getResult() )
            {
                itemized = result.getJSONObject( Data.FIELD.user );
            }
        }
        
        // Ride Search
        else if( url.equals( Data.URL.getRides ) )
        {
            json = Data.currentRide.tojson();
            if( getResult() )
            {
                itemized = result.getJSONObject( Data.FIELD.rides );
                
                Data.MATCHED_RIDES.clear();
                for( int i = 0; i < result.getInt( Data.FIELD.qty ); ++i )
                {
                    Data.MATCHED_RIDES.add( new Ride( itemized.getJSONObject( Data.Empty+i ) ) );
                }
            }
        }
        
        // User Info
        else if( url.equals( Data.URL.getUser ) )
        {
            if( userId > -1 )
            {
                json.put( Data.FIELD.userId, userId );
                json = Data.currentRide.tojson();
                if( getResult() )
                {
                    Data.selectedUser = new User( result.getJSONObject( Data.FIELD.user ) );
                }
            }
        }
        
        // Ride Create
        else if( url.equals( Data.URL.addRide ) )
        {
            json = Data.currentRide.tojson();
            if( getResult() )
            {
                Data.selectedRide = new Ride( result.getJSONObject( Data.FIELD.ride ) );
            }
        }
        
        // Ride Update
        else if( url.equals( Data.URL.updateRide ) )
        {
            json = Data.selectedRide.tojson_addPass( Data.currentUser.ID() );
            if( getResult() )
            {
                Data.selectedRide = new Ride( result.getJSONObject( Data.FIELD.ride ) );
            }
        }
        
        // Ride Update (from pending page)
        else if( url.equals( Data.URL.updateRide ) )
        {
//            json = Globals.selectedRide.tojson_addPass( Globals.currentUser.ID() );
            
            json.put( "rideId", Integer.parseInt( data.getString( "rideId" ) ) );
            json.put( "userId", Integer.parseInt( data.getString( "userId" ) ) );
            json.put( "offering", data.getBoolean( "offering" ) );
            if( getResult() )
            {
                Data.selectedRide = new Ride( result.getJSONObject( Data.FIELD.ride ) );
            }
        }
        
        // Ride Complete
        else if( url.equals( Data.URL.completeRide ) )
        {
            json = Data.selectedRide.tojson();
            Data.selectedRide = null;
            getResult();
        }
        
        // GCM Notifications
        else if( url.equals( Data.URL.gcmout ) && !gcmType.equals( Data.Empty ) )
        {
            if( gcmType.equals( Data.GCM.pending ) )
            {
                // send ride owner 
            }
            else if( gcmType.equals( Data.GCM.complete ) )
            {
                
            }
            
            data.put( Data.FIELD.offering, Data.selectedRide.OFFERING() );
            data.put( Data.FIELD.rideId, Data.selectedRide.ID() );
            data.put( Data.FIELD.userId, Data.currentUser.ID() );
            data.put( Data.FIELD.activity, gcmType );
            
            JSONArray userIds = new JSONArray();
            userIds.put( Data.selectedRide.DRIVERID() );
            for( Integer userId : Data.selectedRide.RIDERIDS() )
            {
                userIds.put( userId );
            }

            json.put( Data.FIELD.toUserId, userIds );
            json.put( Data.FIELD.data, data.toString() );
            getResult();
        }
        
        return result;
    }
    
    private static Boolean getResult() throws ClientProtocolException, IOException, JSONException
    {    
        final int timeoutConnection = 4000,
                  timeoutSocket     = 6000;
        
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout( httpParameters, timeoutConnection );
        HttpConnectionParams.setSoTimeout( httpParameters, timeoutSocket );

        HttpPost httpPost = new HttpPost( url );
        StringEntity stringEntity = new StringEntity( json.toString() );  
        stringEntity.setContentType( new BasicHeader( HTTP.CONTENT_TYPE, "application/json" ) );
        httpPost.setEntity( stringEntity );

        HttpResponse response = new DefaultHttpClient( httpParameters ).execute( httpPost );
        
        if( response != null )
        {
            HttpEntity entity = response.getEntity();
        
            if( !entity.equals( null ) )
            {
                result = new JSONObject( EntityUtils.toString( entity ) );
                Data.verbose = result+Data.nl;
                success = result.getBoolean( Data.FIELD.success );
            }
        }

        return success;
    }
}
