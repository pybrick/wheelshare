package com.cccwheelshare;

import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.res.Resources;
import android.view.View;
import android.widget.TextView;

public class User
{
    private int     id;
    private int     age;
    private String  email;
    private String  username;
    private String  password;
    private String  name;
    private Boolean smoker;
    private double  rating;
    
    private static final String EMAIL_PATTERN = 
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public User()
    {
        this.id       = 0;
        this.age      = 0;
        this.email    = "";
        this.username = "";
        this.password = "";
        this.name     = "";
        this.smoker   = false;
        this.rating   = 0.0;
    }

    public User( JSONObject user ) throws JSONException
    {
        this.id       = user.getInt( "userId" );
        this.age      = user.getInt( "age" );
        this.email    = user.getString( "email" );
        this.username = user.getString( "username" );
        this.password = user.getString( "password" );
        this.name     = user.getString( "name" );
        this.smoker   = user.getBoolean( "smoker" );
        this.rating   = user.getDouble( "rating" );
    }

    public int     ID()                              { return this.id; }
    public void    ID( final int id )                { this.id = id; }
    public int     AGE()                             { return this.age; }
    public void    AGE( final int age )              { this.age = age; }
    public String  EMAIL()                           { return this.email; }
    public void    EMAIL( final String email )       { this.email = email; }
    public String  USERNAME()                        { return this.username; }
    public void    USERNAME( final String username ) { this.username = username; }
    public String  PASSWORD()                        { return this.password; }
    public void    PASSWORD( final String password ) { this.password = password; }
    public String  NAME()                            { return this.name; }
    public void    NAME( final String name )         { this.name = name; }
    public Boolean SMOKER()                          { return this.smoker; }
    public void    SMOKER( final Boolean smoker )    { this.smoker = smoker; }
    public double  RATING()                          { return this.rating; }
    public void    RATING( final double rating )     { this.rating = rating; }
    
    public JSONObject tojson() throws JSONException
    {
        JSONObject user = new JSONObject();
        
        user.put( "id", id );
        user.put( "age", age );
        user.put( "email", email );
        user.put( "username", username );
        user.put( "password", password );
        user.put( "name", name );
        user.put( "smoker", smoker );
        user.put( "rating", rating );
        
        return user;
    }
    
    final public static String checkEmail( final View v )
    {
        Resources r = v.getResources();
        String status = Data.Empty;
        
        if( !( v instanceof TextView ) )
        {
            status += r.getString( R.string.Generic_Bug_TextView )+Data.nl;
        }
        else
        {
            String email = ( (TextView) v ).getText().toString();
            
            if( email.equals( Data.Empty ) )
            {
                status += r.getString( R.string.User_Missing_Email )+Data.nl;
            }
            else if( !User.validate( email ) )
            {
                  status += r.getString( R.string.User_Illegal_Email_Syntax )+Data.nl;
            }
        
            if( status.equals( Data.Empty ) )
            {
                Data.currentUser.EMAIL( email );
            }
        }
        
        return status;
    }
    
    final public static String checkPassword( final View v ) throws NoSuchAlgorithmException
    {
        Resources r = v.getResources();
        String status = Data.Empty;
        
        if( !( v instanceof TextView ) )
        {
            status += r.getString( R.string.Generic_Bug_TextView )+Data.nl;
        }
        else
        {
            String password = ( (TextView) v ).getText().toString();
        
            if( password.equals( Data.Empty ) )
            {
                status += r.getString( R.string.User_Missing_Password )+Data.nl;
            }
            else if( password.length() < 0 )
            {
                status += r.getString( R.string.User_Illegal_Password_Length )+Data.nl;
            }
        }
        
        return status;
    }
    
    final public static String checkHash( final View v ) throws NoSuchAlgorithmException
    {
        Resources r = v.getResources();
        String status = Data.Empty;
        
        if( !( v instanceof TextView ) )
        {
            status += r.getString( R.string.Generic_Bug_TextView )+Data.nl;
        }
        else
        {
            String password = ( (TextView) v ).getText().toString();
        
            if( password.equals( Data.Empty ) )
            {
                status += r.getString( R.string.User_Missing_Password )+Data.nl;
            }
            else if( password.length() < 0 )
            {
                status += r.getString( R.string.User_Illegal_Password_Length )+Data.nl;
            }
        }
        
        return status;
    }
    
    final public static boolean validate( final String email )
    {
        return Pattern.compile( EMAIL_PATTERN ).matcher( email ).matches();
    }
    
    final public String toString()
    {
        return "{"+"ID: "+id+", Age: "+age+", Email: "+email+", Username: "+username+", Password: "+password+", Name: "+name+", Smoker: "+smoker+", Rating: "+rating+"}";
    }
}