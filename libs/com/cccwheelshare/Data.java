package com.cccwheelshare;

import java.util.ArrayList;
import java.util.List;

import android.app.Application;

public class Data extends Application
{
          public  static String     verbose = Data.Empty;
    final public  static String     Empty          = "";
    final public  static String     nl             = "\n";
    final public  static String     SENDER_ID      = "52899871954";
          public  static User       currentUser    = new User();
          public  static User       selectedUser   = new User();
          public  static Ride       currentRide    = new Ride();
          public  static Ride       selectedRide   = new Ride();
          public  static List<Ride> MATCHED_RIDES  = new ArrayList<Ride>();
    final public  static String     CREDITCARD     = "4819133634646832";
    
    public static final String[] CITY_LIST = { "Davis",
                                               "Hayward",
                                               "Fremont",
                                               "Los Angeles",
                                               "Sacramento",
                                               "San Francisco",
                                               "San Jose",
                                               "Vacaville" };
    
    public static String[] SORT_TYPES = { "From city (ascending)",
                                          "From city (descending)",
                                          "To city (ascending)",
                                          "To city (descending)",
                                          "Date/Time (ascending)",
                                          "Date/Time (descending)",
                                          "Cost (ascending)",
                                          "Cost (descending)" };

    final public static class URL
    {
        final private static String ROOT         = "http://cccwheelshare.appspot.com/";
        final public  static String addRide      = ROOT+"/addRide";
        final public  static String completeRide = ROOT+"/completeRide";
        final public  static String gcmout       = ROOT+"/gcmout";
        final public  static String getRides     = ROOT+"/getRides";
        final public  static String getUser      = ROOT+"/getUser";
        final public  static String getUsername  = ROOT+"/getUsername";
        final public  static String Verified_User_FindByEmail   = ROOT+"/Verified_User_FindByEmail";
        final public  static String Unverified_User_FindByEmail = ROOT+"/Unverified_User_FindByEmail";
        final public  static String User_SendVerificationEmail  = ROOT+"/User_SendVerificationEmail";
        final public  static String updateRide   = ROOT+"/updateRide";
    }
    
    final public static class GCM
    {
        final public static String complete = "complete";
        final public static String pending  = "pending";
    }
    
    final public static class FIELD
    {
        final public static String activity = "activity";
        final public static String data     = "data";
        final public static String email    = "email";
        final public static String halt     = "halt";
        final public static String name     = "name";
        final public static String offering = "offering";
        final public static String password = "password";
        final public static String qty      = "qty";
        final public static String ride     = "ride";
        final public static String rideId   = "rideId";
        final public static String rides    = "rides";
        final public static String success  = "success";
        final public static String toUserId = "toUserId";
        final public static String user     = "user";
        final public static String userId   = "userId";
    }
}
