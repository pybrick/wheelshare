package com.cccwheelshare;

import android.os.Bundle;
import android.app.TabActivity;
import android.content.Intent;
import android.view.Menu;
import android.widget.TabHost;

@SuppressWarnings("deprecation")
public class Main extends TabActivity
{
    private static TabHost tabs;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.main );

        tabs = getTabHost();
        tabs.addTab( tabs.newTabSpec( "Ride_Search" ).setIndicator( "Search" ).setContent( new Intent( this, Ride_Search.class ) ) );
        tabs.addTab( tabs.newTabSpec( "Ride_Create" ).setIndicator( "Create" ).setContent( new Intent( this, Ride_Create.class ) ) );
        tabs.addTab( tabs.newTabSpec( "User_Logout" ).setIndicator( "Logout" ).setContent( new Intent( this, User_Logout.class ) ) );
    }

    @Override
    public boolean onCreateOptionsMenu( final Menu menu )
    {
        getMenuInflater().inflate( R.menu.main, menu );
        return true;
    }
    
    public void setTab( final String title )
    {
        tabs.setCurrentTabByTag( title );
    }
    
    @Override
    public void onBackPressed()
    {
        ; // Do nothing
    }
}