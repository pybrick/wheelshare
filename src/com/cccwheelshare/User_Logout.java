package com.cccwheelshare;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;

public class User_Logout extends Activity
{
    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.user_logout );
        confirm();
    }
    
    final private void confirm()
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( this );
        alertDialogBuilder.setTitle( "Logout" );
        alertDialogBuilder.setMessage( "Are you sure you want to logout?" );
        alertDialogBuilder.setCancelable( false );
        alertDialogBuilder.setPositiveButton( "Logout", new DialogInterface.OnClickListener()
        {
            public void onClick( DialogInterface dialog, int id )
            {
                logout();
            }
        });
        alertDialogBuilder.setNegativeButton( "Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick( DialogInterface dialog, int id )
            {
                dialog.cancel();
//                onBackPressed();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    
    final private void logout()
    {
        SharedPreferences sp = getSharedPreferences( "Login", 0 );
        SharedPreferences.Editor Ed = sp.edit();
        Ed.putString( Data.FIELD.email, Data.Empty );
        Ed.putString( Data.FIELD.password, Data.Empty );
        Ed.putBoolean( "isLogin", false );
        Ed.commit();

        startActivity( new Intent( this, User_Login.class ) );
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate( R.menu.user__logout, menu );
        return true;
    }
}
