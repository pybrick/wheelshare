package com.cccwheelshare;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

public class User_Register extends Activity
{
    private static Resources r;
    private String url = Data.Empty;
    
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.user_register );

        Font.setAppFont( (ViewGroup) findViewById( R.id.User_Register ) );
        r = this.getResources();
        Font.Text( findViewById( R.id.EMAIL_Field ), Data.currentUser.EMAIL() );
    }

    public void register( final View v ) throws NoSuchAlgorithmException
    {
        String status = Data.Empty;

        if( Font.Text( findViewById( R.id.NAME_Field ) ).equals( Data.Empty ) )  { status += r.getString( R.string.User_Bad_Name )+"\n"; }
        if( Font.Text( findViewById( R.id.EMAIL_Field ) ).equals( Data.Empty ) ) { status += r.getString( R.string.User_Bad_Email )+"\n"; }
        if( Font.Text( findViewById( R.id.PASS_Field ) ).equals( Data.Empty ) )  { status += r.getString( R.string.User_Bad_Password )+"\n"; }
        if( Font.Text( findViewById( R.id.PASSV_Field ) ).equals( Data.Empty ) ) { status += r.getString( R.string.User_Bad_Password_Verify )+"\n"; }
        else if( !Font.Text( findViewById( R.id.PASS_Field ) ).equals( Font.Text( findViewById( R.id.PASSV_Field ) ) ) ) { status += r.getString( R.string.User_Bad_Password_Match )+"\n"; }
        
        if( status.equals( Data.Empty ) )
        {
            Data.currentUser = new User();
            Data.currentUser.EMAIL( Font.Text( findViewById( R.id.EMAIL_Field ) ) );
            Data.currentUser.NAME( Font.Text( findViewById( R.id.NAME_Field ) ) );
            Data.currentUser.PASSWORD( Encrypt.hashString( Font.Text( findViewById( R.id.PASS_Field ) ) ) );

            new Async().execute( Data.URL.Verified_User_FindByEmail );
        }
        else
        {
            Font.Text( findViewById( R.id.STATUS ), status );
        }
    }
    
    public class Async extends AsyncTask<String, Void, JSONObject>
    {
        protected void onPreExecute() { preAsync(); }
        protected JSONObject doInBackground( final String... URLs )
        {
            try                       { url = URLs[0]; return new JSON().execute( URLs[0] ); }
            catch( ParseException e ) { Except.e = Except.Parse; }
            catch( JSONException e )  { Except.e = Except.JSON; }
            catch( IOException e )    { Except.e = Except.IO; }
            return null;
        }
        protected void onPostExecute( final JSONObject result )
        {
            try                       { postAsync( result ); }
            catch( ParseException e ) { Except.e = Except.Parse; }
            catch( JSONException e )  { Except.e = Except.JSON; }
        }
    }
    
    private void preAsync()
    {
        Font.Text( findViewById( R.id.STATUS ), Data.nl+Data.currentUser.toString() );
        JSON.PROGRESS( this, r.getString( R.string.User_Registering ) );
        JSON.PROGRESS().show();
    }
    
    private void postAsync( final JSONObject result ) throws NotFoundException, JSONException
    {
        JSON.PROGRESS().dismiss();
        
        String status = Data.Empty;

        // Find email address in the verified user table
        if( url.equals( Data.URL.Verified_User_FindByEmail ) )
            if( result == null )
                status += r.getString( R.string.Generic_Try_Again )+" ("+Except.e+")";
            else if( result.getBoolean( Data.FIELD.success ) )
            {
                status += "Email already registered.";
                Data.currentUser.EMAIL( Font.Text( findViewById( R.id.EMAIL_Field ) ) );
                startActivity( new Intent( this, User_Login.class ) );
            }
            else
                new Async().execute( Data.URL.Unverified_User_FindByEmail );

        // Find email address in the unverified user table
        else if( url.equals( Data.URL.Unverified_User_FindByEmail ) )
            if( result == null )
                status += r.getString( R.string.Generic_Try_Again )+" ("+Except.e+")";
            else if( result.getBoolean( Data.FIELD.success ) )
            {
                status += "Email waiting for verification.";
                Data.currentUser.EMAIL( Font.Text( findViewById( R.id.EMAIL_Field ) ) );
                startActivity( new Intent( this, User_Login.class ) );
            }
            else
                if( result.getBoolean( Data.FIELD.halt ) )
                    ;
                else
                    new Async().execute( Data.URL.User_SendVerificationEmail );

        // Send new user a verification email
        else if( url.equals( Data.URL.User_SendVerificationEmail ) )
            if( result == null )
                status += r.getString( R.string.Generic_Try_Again )+" ("+Except.e+")";
            else if( result.getBoolean( Data.FIELD.success ) )
            {
                Font.Text( findViewById( R.id.PASS_Field ), Data.Empty );
                Font.Text( findViewById( R.id.PASSV_Field ), Data.Empty );
                status += r.getString( R.string.Generic_Success )+"\n";
                Data.currentUser.EMAIL( Font.Text( findViewById( R.id.EMAIL_Field ) ) );
                startActivity( new Intent( this, User_Login.class ) );
            }
            else
                ;

        else
            ;

        Font.Text( findViewById( R.id.STATUS ), status+Data.nl+Data.verbose+Data.nl+Data.currentUser.toString() );
    }

    public void login( final View v )
    {
        Data.currentUser.EMAIL( Font.Text( findViewById( R.id.EMAIL_Field ) ) );
        startActivity( new Intent( this, User_Login.class ) );
    }

    public boolean onCreateOptionsMenu( Menu menu )
    {
        getMenuInflater().inflate( R.menu.user_register, menu );
        return true;
    }
}
