package com.cccwheelshare;

import java.io.IOException;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Ride_Pending extends Activity
{
    private TextView INFO;
    private JSONObject data;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.ride_pending );

        Font.setAppFont( (ViewGroup) findViewById( R.id.Ride_Pending ) );
        
        Bundle b = getIntent().getExtras();
        
        INFO = (TextView) findViewById( R.id.Ride_Pending_Text );
        if( b != null ){
            try
            {
                data = new JSONObject(b.getString( "data" ));
                INFO.setText( data.getString("userId") + " " + getApplicationContext().getString(R.string.Pending_Text) + " " + data.getString("rideId") );
            }
            catch( JSONException e )
            {
                INFO.setText( "JSONException" + e );
            }
        }
        else
            INFO.setText( getApplicationContext().getString(R.string.Pending_Error) );
    }
    
    @Override
    protected void onNewIntent (Intent intent){
        
        Bundle b = getIntent().getExtras();
        
        INFO = (TextView) findViewById( R.id.Ride_Pending_Text );
        if(b != null ){
            try
            {
                data = new JSONObject(b.getString( "data" ));
                INFO.setText( data.getString("userId") + " " + getApplicationContext().getString(R.string.Pending_Text) + " " + data.getString("rideId") );
            }
            catch( JSONException e )
            {
                INFO.setText( "JSONException" + e );
            }
        }
        else
            INFO.setText( getApplicationContext().getString( R.string.Pending_Error ) );
        
    }
    
//R.string.Accept_User
    
    public class Async extends AsyncTask<String, Void, JSONObject>
    {
        protected void onPreExecute() { preAsync(); }
        protected JSONObject doInBackground( final String... S )
        {
            try                       { return new JSON().execute( S[0], data ); }
            catch( ParseException e ) { Except.e = Except.Parse; }
            catch( JSONException e )  { Except.e = Except.JSON; }
            catch( IOException e )    { Except.e = Except.IO; }
            return null;
        }
        protected void onPostExecute( final JSONObject result )
        {
            try                       { postAsync( result ); }
            catch( ParseException e ) { Except.e = Except.Parse; }
            catch( JSONException e )  { Except.e = Except.JSON; }
        }
    }
    
    private void preAsync()
    {
        ; // Do nothing
    }
    
    private void postAsync( final JSONObject result ) throws JSONException
    {
        if( result == null )
            ; // Do nothing
        else if( result.getBoolean( Data.FIELD.success ) )
            startActivityForResult( new Intent( this, Ride_Info.class ), 0 );
        else
            ; // Do nothing
    }
    
    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        getMenuInflater().inflate( R.menu.ride_pending, menu );
        return true;
    }
    
    public void accept( final View v )
    {
        if( data != null )
            new Async().execute( Data.URL.updateRide );
            // notify bookee that they have been accepted?
        else
            INFO.setText( "ACCEPT ERROR" );
    }
    
    public void deny( final View v )
    {
        if( data != null )
            // notify bookee that they have been denied?
            Toast.makeText(this, R.string.Deny_User, Toast.LENGTH_LONG).show();
        else
            ; // Do nothing
    }
}
