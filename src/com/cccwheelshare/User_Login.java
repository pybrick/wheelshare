package com.cccwheelshare;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ToggleButton;

public class User_Login extends Activity
{
    private static Resources r;

    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.user_login );

        ( (ToggleButton) findViewById( R.id.Change_Font ) ).setChecked( Font.status );  
        Font.setAppFont( (ViewGroup) findViewById( R.id.User_Login ) );
        r = this.getResources();
        Font.Text( findViewById( R.id.EMAIL_Field ), Data.currentUser.EMAIL() );
        
        SharedPreferences login = getSharedPreferences( "Login", -1 );
        
        Boolean isLogin = login.getBoolean( "isLogin", false );
        
        if( isLogin )
        {
            String email = login.getString( Data.FIELD.email, Data.Empty );
            Font.Text( findViewById( R.id.EMAIL_Field ), email );
            
            try
            {
                login( login.getString( Data.FIELD.password, Data.Empty ) );
            }
            catch( NoSuchAlgorithmException e )     { Except.e = Except.NoSuchAlgorithm; }
            catch( UnsupportedEncodingException e ) { Except.e = Except.UnsupportedEncoding; }
        }
    }

    public void login( final View v ) throws NoSuchAlgorithmException, UnsupportedEncodingException
    {
        login( Data.Empty );
    }

    public void login( final String password ) throws NoSuchAlgorithmException, UnsupportedEncodingException
    {
        String status = Data.Empty;

        status += User.checkEmail( findViewById( R.id.EMAIL_Field ) );
        
        if( password.equals( Data.Empty ) )
            status += User.checkPassword( findViewById( R.id.PASS_Field ) );

        if( status.equals( Data.Empty ) )
            new Async().execute( Data.URL.Verified_User_FindByEmail, password );
        else
        {
            status = r.getString( R.string.Generic_Try_Again )+Data.nl+status;
            Font.Text( findViewById( R.id.STATUS ), status );
        }
    }
    
    public void guest_login( final View v ) throws NoSuchAlgorithmException, UnsupportedEncodingException
    {
        startActivity( new Intent( this, Main.class ) );
    }
    
    private void preAsync()
    {
        JSON.PROGRESS( this, r.getString( R.string.User_Logging_In ) );
        JSON.PROGRESS().show();
    }
    
    public class Async extends AsyncTask<String, Void, JSONObject>
    {
        private String password;

        protected void onPreExecute()
        {
            preAsync();
        }
        protected JSONObject doInBackground( final String... S )
        {
            password = S[1];
            
            try                       { return new JSON().execute( S[0] ); }
            catch( IOException e )    { Except.e = Except.IO; }
            catch( JSONException e )  { Except.e = Except.JSON; }
            catch( ParseException e ) { Except.e = Except.Parse; }
            return null;
        }
        protected void onPostExecute( final JSONObject result )
        {
            try                                 { postAsync( result, password ); }
            catch( JSONException e )            { Except.e = Except.JSON; }
            catch( NoSuchAlgorithmException e ) { Except.e = Except.NoSuchAlgorithm; }
            catch( NotFoundException e )        { Except.e = Except.NotFound; }
            catch( ParseException e )           { Except.e = Except.Parse; }
        }
    }
    
    private void postAsync( final JSONObject result, String password ) throws NoSuchAlgorithmException, NotFoundException, JSONException
    {
        JSON.PROGRESS().dismiss();
        
        String status = Data.Empty;
        if( result == null )
            status += r.getString( R.string.Generic_Try_Again )+" ("+Except.e+")";
        else if( result.getBoolean( Data.FIELD.success ) )
        {
            if( password.equals( Data.Empty ) )
            {
                password = Encrypt.hashString( Font.Text( findViewById( R.id.PASS_Field ) ) );
                Font.Text( findViewById( R.id.PASS_Field ), Data.Empty );
            }
            
            if( password.equals( result.getString( Data.FIELD.password ) ) )
            {
                status += r.getString( R.string.Generic_Success )+Data.nl;
                
                SharedPreferences sp = getSharedPreferences( "Login", 0 );
                SharedPreferences.Editor Ed = sp.edit();
                Ed.putString( Data.FIELD.email, result.getString( Data.FIELD.email ) );
                Ed.putString( Data.FIELD.password, result.getString( Data.FIELD.password ) );
                Ed.putBoolean( "isLogin", true );
                Ed.commit();
                
                Data.currentUser = new User( result );
                
                startActivity( new Intent( this, Main.class ) );
            }
            else
                status += r.getString( R.string.Generic_Failure )+Data.nl+
                          r.getString( R.string.Generic_Try_Again )+Data.nl;
        }
        else
            status += r.getString( R.string.Generic_Try_Again )+Data.nl+
                      r.getString( R.string.User_FindByEmail_False )+Data.nl;

//        status += result;
        Font.Text( findViewById( R.id.STATUS ), status );
    }
    
    public void toggleFont( final View v )
    {
        Font.status = ( (ToggleButton) findViewById( R.id.Change_Font ) ).isChecked();
        startActivity( new Intent( this, User_Login.class ) );
    }

    public void register( final View v )
    {
        Data.currentUser.EMAIL( Font.Text( findViewById( R.id.EMAIL_Field ) ) );
        startActivity( new Intent( this, User_Register.class ) );
    }
    
    public boolean onCreateOptionsMenu( Menu menu )
    {
        getMenuInflater().inflate( R.menu.user_login, menu );
        return true;
    }
    
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent( Intent.ACTION_MAIN );
        intent.addCategory( Intent.CATEGORY_HOME );
        intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
        startActivity( intent );
    }
}
