package com.cccwheelshare;

import java.io.IOException;

import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class Ride_Info extends FragmentActivity
{
    private static TextView  BOOK;
    private static TextView  COMPLETE;
    private static Resources r;
    private static TextView  RIDE_INFO;
    private static String    status;
    private static TextView  STATUS;

    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.ride_info );

        Font.setAppFont( (ViewGroup) findViewById( R.id.Ride_Info ) );

        BOOK      = (Button)   findViewById( R.id.BOOK_Button );
        COMPLETE  = (Button)   findViewById( R.id.COMPLETE_RIDE_Button );
        r         = this.getResources();
        RIDE_INFO = (TextView) findViewById( R.id.RIDE_INFO_Text );
        STATUS    = (TextView) findViewById( R.id.STATUS );

        toggle();
    }
    
    private void toggle()
    {
        status                   = Data.Empty;
        String  book             = Data.Empty;
        Boolean book_visible     = null;
        Boolean complete_visible = null; 
        
        Integer driverId = Data.selectedRide.DRIVERID();
        Integer userId   = Data.currentUser.ID();
        
        if( driverId.equals( userId ) )
        {
            status          += "\n"+r.getString( R.string.Ride_Info_Owned_Text );
            book_visible     = false;
            complete_visible = Data.selectedRide.OFFERING(); 
        }
        else
        {
            complete_visible = false;

            if( Data.selectedRide.OFFERING() )
            {
                if( Data.selectedRide.hasRider( Data.currentUser.ID() ) )
                {
                    status      += "\n"+r.getString( R.string.Ride_Info_Already_Booked );
                    book_visible = false;
                }
                else
                {
                    book         = r.getString( R.string.Ride_Info_Book_Button );
                    book_visible = true;
                }
            }
            else
            {
                book         = r.getString( R.string.Ride_Info_Fulfill_Button );
                book_visible = true;
            }
        }
        
        if( book_visible.equals( null ) )
            status += "\n"+r.getString( R.string.Ride_Book_Bad_Visibility );
        BOOK.setVisibility( book_visible ? View.VISIBLE : View.GONE );
        BOOK.setText( book );

        if( complete_visible.equals( null ) )
            status += "\n"+r.getString( R.string.Ride_Bad_Complete_Visibility );
        COMPLETE.setVisibility( complete_visible ? View.VISIBLE : View.GONE );
        COMPLETE.setText( r.getString( R.string.Ride_Complete_Button ) );

        RIDE_INFO.setText( Data.selectedRide.printPretty() );
        STATUS.setText( status );
    }
    
    public void bookRide( final View v ) throws ClientProtocolException, JSONException, IOException
    {
        if( Data.currentRide.OFFERING() )
            startActivityForResult( new Intent( this, Ride_Payment.class ), 0 );
        else
            startActivityForResult( new Intent( this, Ride_Booking.class ), 0 );
    }
    
    public void completeRide( final View v )
    {
        STATUS.setText( Data.Empty );
        new Async().execute( Data.URL.completeRide );
    }
    
    public class Async extends AsyncTask<String, Void, JSONObject>
    {
        protected void onPreExecute() { preAsync(); }
        protected JSONObject doInBackground( final String... S )
        {
            try                       { return new JSON().execute( S[0] ); }
            catch( ParseException e ) { Except.e = Except.Parse; }
            catch( JSONException e )  { Except.e = Except.JSON; }
            catch( IOException e )    { Except.e = Except.IO; }
            return null;
        }
        protected void onPostExecute( final JSONObject result )
        {
            try                       { postAsync( result ); }
            catch( JSONException e )  { Except.e = Except.JSON; }
            catch( ParseException e ) { Except.e = Except.Parse; }
        }
    }
    
    private void preAsync()
    {
        ; // Do nothing
    }
    
    private void postAsync( final JSONObject result ) throws NotFoundException, JSONException
    {
        if( result == null )
            status += "\n"+Except.e;
        if( result.getBoolean( Data.FIELD.success ) )
        {
            status += "\n"+r.getString( R.string.Generic_Success );
            finish();
            if( Data.selectedRide == null )
                startActivity( new Intent( this, Main.class ) );
            else
                startActivity( getIntent() );
        }
        else
            status += "\n"+r.getString( R.string.Generic_Failure );
        STATUS.setText( status );
    }
    
    public boolean onCreateOptionsMenu( Menu menu )
    {
        getMenuInflater().inflate( R.menu.ride_info, menu );
        return true;
    }
}
