package com.cccwheelshare;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class Ride_Search_Results extends Activity
{
    private static ListView  LISTVIEW;
    private static Resources r;
    private static EditText  SORT;
    private static TextView  STATUS;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.ride_search_results );

        Font.setAppFont( (ViewGroup) findViewById( R.id.Ride_Search_Results ) );

        LISTVIEW = (ListView) findViewById( R.id.RESULTS_ListView );
        r        = this.getResources();
        SORT     = (EditText) findViewById( R.id.SORT_BY_FIELD );
        STATUS   = (TextView) findViewById( R.id.STATUS );
    }
    
    protected void onResume()
    {
        super.onResume();
        
        if( Data.MATCHED_RIDES.isEmpty() )
            // No rides found
            STATUS.setText( r.getString( R.string.Ride_Search_Results_None ) );
        else
        {
            // Rides found
            STATUS.setText( Data.MATCHED_RIDES.size()+" "+r.getString( R.string.Ride_Search_Found_Postfix ) );
            try                       { displayResults(); }
            catch( ParseException e ) { Except.e = Except.Parse; }
            catch( JSONException e )  { Except.e = Except.JSON; }
            catch( IOException e )    { Except.e = Except.IO; }
        }
    }

    public void createNewRide( final View v )
    {
        startActivityForResult( new Intent( this, Ride_Create.class ), 0 );
    }
    
    /*
     * Sorting interfaces - A Callable interface provides a common way to access implementing sorting methods
     * The classes that follow implements Callable for a specific comparison logic to feed to the sorting alg
     */
    private static interface Callable
    {
        public int compare( Ride A, Ride B );
    }
    
    // Sort by source city
    private static class SortBySrc implements Callable
    {
        public int compare( Ride A, Ride B ) { return A.SRC().compareTo( B.SRC() ); }
    }
    
    // Sort by destination city
    private static class SortByDest implements Callable
    {
        public int compare( Ride A, Ride B ) { return A.DEST().compareTo( B.DEST() ); }
    }
    
    // Sort by departure time 
    private static class SortByTime implements Callable
    {
        public int compare( Ride A, Ride B ) { return A.STARTTIME().compareTo( B.STARTTIME() ); }
    }
    
    // Sort by ride cost
    private static class SortByCost implements Callable
    {
        public int compare( Ride A, Ride B ) { return A.COST() < B.COST() ? -1
                                                                          : ( A.COST() > B.COST() ) ? 1 
                                                                                                    : 0; }
    }
    
    /*
     * Sorts the MATCHED_RIDES list based on a passed function.
     * Function MUST compare two rides A and B and return:
     * < 0: A < B
     * > 0: A > B
     * = 0: A = B
     * 
     * Of course, different functions will be used depending on the required sorting
     */
    private static void selectSort( final Boolean descend, final Callable func )
    {
        int mindex;
        int res;
        
        for( int i = 0; i < Data.MATCHED_RIDES.size() - 1; i++ )
        {
            mindex = i;
            for( int j = i + 1; j < Data.MATCHED_RIDES.size(); j++ )
            {
                res = func.compare( Data.MATCHED_RIDES.get( mindex ), Data.MATCHED_RIDES.get( j ) );
                // descend and A < B || ascend and A > B - mark to swap
                if( ( descend && ( res < 0 ) ) || ( !descend && ( res > 0 ) ) )
                    mindex = j;
            }
            
            if( mindex != i )
            {
                Ride temp = Data.MATCHED_RIDES.get( i );
                Data.MATCHED_RIDES.set( i, Data.MATCHED_RIDES.get( mindex ) );
                Data.MATCHED_RIDES.set( mindex, temp );
            }
        }
    }
    
    private AlertDialog createAlertDialog( AlertDialog.Builder listBuilder,
                                           final CharSequence[] items,
                                           final String title,
                                           final EditText editText )
    {
        listBuilder.setTitle( title );
        listBuilder.setItems( items,
              new DialogInterface.OnClickListener()
              {
                  public void onClick( DialogInterface dialog, int item )
                  {
                      editText.setId( item );
                      editText.setText( items[item] );
                      final Boolean descend = true;
                      switch( item )
                      {
                          case 0: selectSort( !descend, new SortBySrc() );  break;
                          case 1: selectSort(  descend, new SortBySrc() );  break;
                          case 2: selectSort( !descend, new SortByDest() ); break;
                          case 3: selectSort(  descend, new SortByDest() ); break;
                          case 4: selectSort( !descend, new SortByTime() ); break;
                          case 5: selectSort(  descend, new SortByTime() ); break;
                          case 6: selectSort( !descend, new SortByCost() ); break;
                          case 7: selectSort(  descend, new SortByCost() ); break;
                      }
                    
                      try                       { displayResults(); }
                      catch( ParseException e ) { Except.e = Except.Parse; }
                      catch( JSONException e )  { Except.e = Except.JSON; }
                      catch( IOException e )    { Except.e = Except.IO; }
                  }
              }
        );

        return listBuilder.create();
    }

    public void sortBy( final View v )
    {
        createAlertDialog( new AlertDialog.Builder( this ),
                           Data.SORT_TYPES,
                           r.getString( R.string.Ride_Search_Results_Sort_Title ),
                           SORT ).show();
    }
    
    private void displayResults() throws ClientProtocolException, JSONException, IOException
    {
        List<String> list = new ArrayList<String>();
        
        for( Ride ride : Data.MATCHED_RIDES )
        {
            String src     = ride.SRC();
            String dest    = ride.DEST();
            String prefix  = Data.Empty;
            String infix   = Data.Empty;
            String postfix = Data.Empty;
            
            if( !src.equals( Data.Empty ) && !dest.equals( Data.Empty ) )
                infix = " "+r.getString( R.string.Ride_Search_Results_Both )+" ";
            else if( src.equals( Data.Empty ) && !dest.equals( Data.Empty ) )
                prefix = r.getString( R.string.Ride_Search_Results_Dest )+" ";
            else if( !src.equals( Data.Empty ) &&  dest.equals( Data.Empty ) )
                prefix = r.getString( R.string.Ride_Search_Results_Src )+" ";
            else
                prefix = r.getString( R.string.Ride_Search_Results_Neither );
            
            String smoking = ride.SMOKING() ? r.getString( R.string.Ride_Search_Smoking_True )
                                            : r.getString( R.string.Ride_Search_Smoking_False );
            
            String driver = Data.Empty;
            
            Boolean getUsersFromDatastore = false;
            if( getUsersFromDatastore )
                driver = Data.selectedUser.NAME()+" ("+Data.selectedUser.EMAIL()+")";
            else
                if( ride.DRIVERID() == Data.currentUser.ID() )
                    driver = Data.currentUser.NAME()+" ("+Data.currentUser.EMAIL()+")";
                else
                    driver = "<"+ride.DRIVERID()+">";

            String cost = ((Double) ride.COST()).equals( 0.0 ) ? r.getString( R.string.Ride_Cost_Free )
                                                               : r.getString( R.string.Ride_Create_Cost_Hint )+String.format("%.2f", ride.COST());
            
            list.add( prefix+src+infix+dest+postfix+
                      "\n"+cost+" ("+smoking+")"+
                      "\n"+r.getString( R.string.User )+" "+driver );
        }
        
        if( !list.equals( null ) )
            populateListView( list );
    }
    
    private void populateListView( List<String> list )
    {
        LISTVIEW.setId( 1 );
        LISTVIEW.setAdapter( new StableArrayAdapter( this,
                                                     android.R.layout.simple_list_item_1,
                                                     list ) );

        LISTVIEW.setOnItemClickListener( new AdapterView.OnItemClickListener()
        {
            public void onItemClick( AdapterView<?> parent,
                                     final View view,
                                     int position,
                                     long id )
            {
                Data.selectedRide = Data.MATCHED_RIDES.get( position );
                startActivityForResult( new Intent( view.getContext(), Ride_Info.class ), 0 );
            }
        } );
    }

    private class StableArrayAdapter extends ArrayAdapter<String>
    {
        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter( Context context, int textViewResourceId, List<String> objects )
        {
            super( context, textViewResourceId, objects );

            for( int i = 0; i < objects.size(); ++i )
                mIdMap.put( objects.get( i ), i );
        }

        public long getItemId( final int position )
        {
            return mIdMap.get( getItem( position ) );
        }

        public boolean hasStableIds()
        {
            return true;
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        getMenuInflater().inflate( R.menu.ride_search_results, menu );
        return true;
    }
}
