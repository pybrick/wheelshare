package com.cccwheelshare;

import java.io.IOException;
import java.util.Date;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;

public class Ride_Create extends FragmentActivity
{
    private static EditText   COST;
    private static EditText   DATE;
    private static EditText   DEST;
    private static EditText   MAXPASS;
    private static TableRow   MAXPASS_Row;
    private static RadioGroup OFFER;
    private static Resources  r;
    private static CheckBox   SMOKING;
    private static EditText   SRC;
    private static String     status;
    private static TextView   STATUS;
    private static EditText   TIME;

    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.ride_create );

        Font.setAppFont( (ViewGroup) findViewById( R.id.Ride_Create ) );

        COST        = (EditText)   findViewById( R.id.COST_Field );
        DATE        = (EditText)   findViewById( R.id.DEPART_DATE );
        DEST        = (EditText)   findViewById( R.id.DEST_Field );
        MAXPASS     = (EditText)   findViewById( R.id.MAXPASS_Field );
        MAXPASS_Row = (TableRow)   findViewById( R.id.MAXPASS_Row );
        OFFER       = (RadioGroup) findViewById( R.id.OFFER_Radio );
        r           = getResources();
        SMOKING     = (CheckBox)   findViewById( R.id.SMOKER_CheckBox );
        SRC         = (EditText)   findViewById( R.id.SRC_Field );
        STATUS      = (TextView)   findViewById( R.id.STATUS );
        TIME        = (EditText)   findViewById( R.id.DEPART_TIME );

        SRC.setText( Data.currentRide.SRC() );
        DEST.setText( Data.currentRide.DEST() );
        COST.setText( ((Double) Data.currentRide.COST()).toString() );
        SMOKING.setChecked( Data.currentRide.SMOKING() );
//      TIME.setText( global.currentRide.STARTTIME() );
//      DATE.setText( global.currentRide.STARTTIME() );
        
        offerSwitch( null );
    }

    public void offerSwitch( final View v )
    {
        // Clear the status text
        status = Data.Empty;

        // Check if user is offering or requesting a ride
        Integer offer = OFFER.getCheckedRadioButtonId();
        if( offer.equals( R.id.OFFER_Driver ) )
            // Show max passenger field
            MAXPASS_Row.setVisibility( View.VISIBLE );
        else if( offer.equals( R.id.OFFER_Passenger ) )
            // Hide max passenger field
            MAXPASS_Row.setVisibility( View.GONE );
        else
            // Something went seriously wrong with the radio buttons
            status += "\n"+r.getString( R.string.Generic_Bad_RadioButton );
        
        // Show the status text
        STATUS.setText( status );
    }
    
    public void cityLookup( final View v )
    {
        // Clear the status text 
        status = Data.Empty;
        
        EditText CITY = null;
        
        if( v.getId() == R.id.SRC_Field )
            // Called by source city
            CITY = SRC;
        else if( v.getId() == R.id.DEST_Field )
            // Called by destination city
            CITY = DEST;
        else
            // Possibly called by non- source/destination city
            status += "\n"+r.getString( R.string.Ride_Bad_City_Field );
        
        if( status.equals( Data.Empty ) )
        {
            // Prompt user for city selection
            Helper.createAlertDialog( new AlertDialog.Builder( this ),
                                      // TODO --Store contents of CITY_LIST in a db table called CITY_LIST
                                      Data.CITY_LIST,
                                      r.getString( R.string.Ride_City_Title ),
                                      CITY ).show();
        }
        else
            STATUS.setText( status );
    }
    
    public void showDatePickerDialog( final View v )
    {
        // Show DatePicker Dialog
        new DatePickerFragment( DATE ).show( getSupportFragmentManager(), "datePicker" );
    }
        
    public void showTimePickerDialog( final View v )
    {
        // Show TimePicker Dialog
        new TimePickerFragment( TIME ).show( getSupportFragmentManager(), "timePicker" );
    }

    @SuppressWarnings( "deprecation" )
    public void createSubmit( final View v )
    {
        status = Data.Empty;
        
        // Check source city
        String src = SRC.getText().toString().trim();
        if( src.equals( Data.Empty ) )
            status += "\n"+r.getString( R.string.Ride_Bad_Src_Missing );

        // Check destination city
        String dest = DEST.getText().toString().trim();
        if( dest.equals( Data.Empty ) )
            status += "\n"+r.getString( R.string.Ride_Bad_Dest_Missing );

        // Check cost
        Double cost    = 0.0;
        String costStr = COST.getText().toString().trim();
        if( costStr.equals( Data.Empty ) )
            status += "\n"+r.getString( R.string.Ride_Bad_Cost_Missing );
        else
        {
            cost = Double.parseDouble( costStr );
            if( cost < 0 )
                status += "\n"+r.getString( R.string.Ride_Bad_Cost_Invalid );
        }

        Integer offer    = OFFER.getCheckedRadioButtonId();
        Integer maxPass  = 1;
        Boolean offering = null;
        
        if( offer.equals( R.id.OFFER_Driver ) )
        {
            String maxPassStr = MAXPASS.getText().toString().trim();
            offering = true;
            if( maxPassStr.equals( Data.Empty ) )
                status += "\n"+r.getString( R.string.Ride_Bad_maxPass_Missing );
            else
            {
                maxPass = Integer.parseInt( maxPassStr );
                if( maxPass <= 0 )
                    status += "\n"+r.getString( R.string.Ride_Bad_maxPass_Invalid );
            }
        }
        else if( offer.equals( R.id.OFFER_Passenger ) )
        {
            offering = false;
            maxPass  = 1;
        }
        else
            status += "\n"+r.getString( R.string.Generic_Bad_RadioButton );
        
        // check date and time
        Date   startTime = new Date();
        String date      = DATE.getText().toString().trim();
        String time      = TIME.getText().toString().trim();
        if( date.equals( Data.Empty ) && time.equals( Data.Empty ) )
            // TODO --Add the following to strings.xml
            status += "\n"+"Date and time are both empty.";
        else
            startTime = new Date( date+ " " +time );

        if( status.equals( Data.Empty ) )
        {
            // No errors, create a new ride
            Data.currentRide = new Ride();
            Data.currentRide.DRIVERID( Data.currentUser.ID() );
            Data.currentRide.SRC( src );
            Data.currentRide.DEST( dest );
            Data.currentRide.SMOKING( SMOKING.isChecked() );
            Data.currentRide.OFFERING( offering );
            Data.currentRide.MAXPASS( maxPass );
            Data.currentRide.REMPASS( maxPass );
            Data.currentRide.COST( cost );
            Data.currentRide.STARTTIME( startTime );

            // Begin Datastore communication
            new Async().execute( Data.URL.addRide );
        }
        else
            // Error occurred: show status text
            STATUS.setText( status );
    }
    
    public class Async extends AsyncTask<String, Void, JSONObject>
    {
        protected void onPreExecute() { preAsync(); }
        protected JSONObject doInBackground( final String... S )
        {
            try                       { return new JSON().execute( S[0] ); }
            catch( ParseException e ) { Except.e = Except.Parse; }
            catch( JSONException e )  { Except.e = Except.JSON; }
            catch( IOException e )    { Except.e = Except.IO; }
            return null;
        }
        protected void onPostExecute( final JSONObject result )
        {
            try                       { postAsync( result ); }
            catch( JSONException e )  { Except.e = Except.JSON; }
            catch( ParseException e ) { Except.e = Except.Parse; }
        }
    }
    
    private void preAsync()
    {
        STATUS.setText( r.getString( R.string.Ride_Creating ) );
    }
    
    private void postAsync( final JSONObject result ) throws NotFoundException, JSONException
    {
        if( result == null )
            // Exception occurred
            STATUS.setText( Except.e );
        else if( result.getBoolean( Data.FIELD.success ) )
        {
            // Ride created successfully, proceed to ride information page
            STATUS.setText( r.getString( R.string.Ride_Create_Success ) );
            startActivityForResult( new Intent( this, Ride_Info.class ), 0 );
        }
        else
            // Failed to create the ride
            STATUS.setText( r.getString( R.string.Ride_Create_Failed ) );
    }
    
    public boolean onCreateOptionsMenu( Menu menu )
    {
        getMenuInflater().inflate( R.menu.ride_create, menu );
        return true;
    }
}