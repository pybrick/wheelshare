package com.cccwheelshare;

import java.io.IOException;
import java.util.Date;

import org.apache.http.ParseException;

import org.json.JSONException;
import org.json.JSONObject;

//import com.google.android.gcm.GCMRegistrar;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.AlertDialog;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;

public class Ride_Search extends FragmentActivity
{
    private static EditText   DATE;
    private static EditText   DEST;
    private static RadioGroup OFFER;
    private static Resources  r;
    private static CheckBox   SMOKING;
    private static EditText   SRC;
    private static Date       startTime;
    private static String     status;
    private static TextView   STATUS;
    private static EditText   TIME;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.ride_search );

        Font.setAppFont( (ViewGroup) findViewById( R.id.Ride_Search ) );

        r       = this.getResources();
        DATE    = (EditText)   findViewById( R.id.DEPART_DATE );
        DEST    = (EditText)   findViewById( R.id.DEST_Field );
        OFFER   = (RadioGroup) findViewById( R.id.OFFER_Radio );
        SMOKING = (CheckBox)   findViewById( R.id.SMOKING_CheckBox );
        SRC     = (EditText)   findViewById( R.id.SRC_Field );
        STATUS  = (TextView)   findViewById( R.id.STATUS );
        TIME    = (EditText)   findViewById( R.id.DEPART_TIME );
        
        // Device registration
//        STATUS.setText( Helper.registerDevice( this ) );
    }

    public void cityLookup( final View v )
    {
        status = Data.Empty;
        EditText CITY = null;
        
        if( v.getId() == R.id.SRC_Field )
            CITY = SRC;
        else if( v.getId() == R.id.DEST_Field )
            CITY = DEST;
        else
            status += "\n"+r.getString( R.string.Ride_Bad_City_Field );

        if( status.equals( Data.Empty ) && !CITY.equals( null ) )
            Helper.createAlertDialog( new AlertDialog.Builder( this ),
                                     Data.CITY_LIST,
                                     r.getString( R.string.Ride_City_Title ),
                                     CITY ).show();
        else
            STATUS.setText( status );
    }
    
    public void showDatePickerDialog( final View v )
    {
        new DatePickerFragment( DATE ).show( getSupportFragmentManager(), "datePicker" );
    }
        
    public void showTimePickerDialog( final View v )
    {
        new TimePickerFragment( TIME ).show( getSupportFragmentManager(), "timePicker" );
    }

    @SuppressWarnings( "deprecation" )
    public void searchSubmit( final View v )
    {
        status = Data.Empty;

        Integer offer    = OFFER.getCheckedRadioButtonId();
        Boolean offering = null;
        if( offer.equals( R.id.OFFER_Driver ) )         { offering = false; }
        else if( offer.equals( R.id.OFFER_Passenger ) ) { offering = true; }
        else                                            { status += "\n"+r.getString( R.string.Generic_Bad_RadioButton ); }

        // TODO Need to match DATE and TIME separately. Currently creates a new startTime if either is blank. Not good enough.
        String date = DATE.getText().toString().trim();
        String time = TIME.getText().toString().trim();
        if( time.equals( Data.Empty ) && date.equals( Data.Empty ) )
            startTime = new Date();
        else
            startTime = new Date( date+ " " +time );
        
        if( status.equals( Data.Empty ) )
        {
            Data.currentRide = new Ride();
            Data.currentRide.SRC( SRC.getText().toString().trim() );
            Data.currentRide.DEST( DEST.getText().toString().trim() );
            Data.currentRide.SMOKING( SMOKING.isChecked() );
            Data.currentRide.OFFERING( offering );
            Data.currentRide.STARTTIME( startTime );
            
            new Async().execute( Data.URL.getRides );
        }
        else
            STATUS.setText( status );
    }

    
    /*** Async ***/
    public class Async extends AsyncTask<String, Void, JSONObject>
    {
        protected void onPreExecute() { preAsync(); }
        protected JSONObject doInBackground( final String... S )
        {
            try                       { return new JSON().execute( S[0] ); }
            catch( ParseException e ) { Except.e = Except.Parse; }
            catch( JSONException e )  { Except.e = Except.JSON; }
            catch( IOException e )    { Except.e = Except.IO; }
            return null;
        }
        protected void onPostExecute( final JSONObject result )
        {
            try                       { postAsync( result ); }
            catch( ParseException e ) { Except.e = Except.Parse; }
            catch( JSONException e )  { Except.e = Except.JSON; }
        }
    }
    
    private void preAsync()
    {
        STATUS.setText( r.getString( R.string.Ride_Searching ) );
    }
    
    private void postAsync( final JSONObject result ) throws NotFoundException, JSONException
    {
        if( result == null )
            STATUS.setText( Except.e );
        else if ( result.getBoolean( Data.FIELD.success ) )
        {
            STATUS.setText( r.getString( R.string.Ride_Search_Success_Prefix )+" "+
                            Data.MATCHED_RIDES.size()+" "+
                            r.getString( R.string.Ride_Search_Success_Postfix ) );

            startActivityForResult( new Intent( this, Ride_Search_Results.class ), 0 );
        }
        else
        {
            STATUS.setText( r.getString( R.string.Ride_Search_Results_None ) );
            startActivityForResult( new Intent( this, Ride_Search_Results.class ), 0 );
        }
    }

    
    @Override
    public boolean onCreateOptionsMenu( final Menu menu )
    {
        getMenuInflater().inflate( R.menu.ride_search, menu );
        return true;
    }
    
    @Override
    public void onBackPressed()
    {
        ; // Do nothing
    }
}
