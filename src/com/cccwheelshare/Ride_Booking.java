package com.cccwheelshare;

import java.io.IOException;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

// TODO --add strings to strings.xml
public class Ride_Booking extends Activity
{
    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.ride_payment );

//        new Async().execute( Strings.URL.gcmout );
        new Async().execute( Data.URL.updateRide );
    }
    
    public class Async extends AsyncTask<String, Void, JSONObject>
    {
        protected void onPreExecute() { preAsync(); }
        protected JSONObject doInBackground( final String... S )
        {
            try                       { return new JSON().execute( S[0] ); }
            catch( ParseException e ) { Except.e = Except.Parse; }
            catch( JSONException e )  { Except.e = Except.JSON; }
            catch( IOException e )    { Except.e = Except.IO; }
            return null;
        }
        protected void onPostExecute( final JSONObject result )
        {
            try                       { postAsync( result ); }
            catch( ParseException e ) { Except.e = Except.Parse; }
            catch( JSONException e )  { Except.e = Except.JSON; }
        }
    }
    
    private void preAsync()
    {
        ; // Do nothing
    }
    
    private void postAsync( final JSONObject result ) throws JSONException
    {
        if( result == null )
            // TODO --exception occurred?
            ; // Do nothing
        else if( result.getBoolean( Data.FIELD.success ) )
        {
            setResult( RESULT_OK, new Intent().putExtra( "success", true ).putExtra( "message", "Ride booked" ) );
            finish();
        }
        else
            // TODO --Booking failed?
            ; // Do nothing
    }
    
    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        getMenuInflater().inflate( R.menu.ride_payment, menu );
        return true;
    }
}
