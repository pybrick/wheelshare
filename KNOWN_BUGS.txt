Known Bugs:
These are the known bugs in the current iteration of the system, as known by the development team. They are documented below:

Domino effect of errors:
When one error occurs in the app, it will cause a cascade of more errors, causing unexpected results in the behavior of the app. For example, a JSONException in the app will cause the app to abruptly end, instead of reporting the JSONException and moving through the necessary pages. We did not have enough time to properly handle these errors, but it may be due to the fact that we are using a Globals class. This is not a part of any use-case.

Multiple accept/deny openings:
When a user receives an accept/deny notification while on the accept/deny page (and have not made a choice), the first request will be dropped. This can be seen as appropriate behavior (a choice not being made is equivalent to a �deny�). This is  a part of the �Ride Search� and �Ride Join� use cases. We tried to check this via verbose debugging output that details what local variables are in what state when Lifecycle calls, like onResume(), are made. If a pending request was loaded, then another was started via the notification message, the first is overwritten by the second and is unrecoverable.

Activity call to Ride_Create() loses the tabs bar:
When an activity call to �Ride_Search()� is made, then the tabs above the ride search option disappear. A possible fix is the following: change �start new (this, blank.class)� to �start new (tabs.class, blank.class)�. This was not a part of the use case, but it is an internal bug specific to our implementation. This is unfortunate if a user wanted to create a ride form a failed search, as the tab bars allowed them to access the Ride Search page again (which they cannot do with the absence of these tabs).

Multi-cast Google Cloud Messaging does not work:
We cannot properly pass an array of ids via JSON to the backend to then extract all the IDs we would want to send to. Either the python backend took a list of a single ID and turned it into a single number (no list) which would become un-iterable in the Python code, or we would receive a list of lists which we did not have enough time to try and work around.
Debugging code is still within the code:
Debugging code is not removed, but should not display because we toggle it with a  debug variable.

Localization with languages does not switch within the app:
This is machine-dependent, but we cannot toggle between English and Hindi translation in the app. We can have one or the either, and we can enable Unicode support if the device does not naturally support it, but there is no physical toggle for the user to translate form Hindi to English. 
